package ru.education.client.menu;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.*;
import ru.education.client.threads.user.UserThread;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConsoleMenuTest {

    Command commandSpy;
    UserThread userThreadMock;

    @BeforeTest
    private void createMocks() {
        commandSpy = Mockito.mock(Command.class);

        userThreadMock = Mockito.mock(UserThread.class);
    }

    @AfterTest
    private void destroyMocks() {
        Mockito.reset(commandSpy);
        Mockito.reset(userThreadMock);
    }

    @Test
    private void checkExisting() {
        ConsoleMenu consoleMenu = new ConsoleMenu(commandSpy);
        Assert.assertNotNull(consoleMenu);
    }

    @Test
    private void checkRunMethod() {
        ConsoleMenu consoleMenu = new ConsoleMenu(commandSpy);
        consoleMenu.run(userThreadMock);

        verify(commandSpy).execute(userThreadMock);
    }

    @Test
    private void checkConsoleMenuExit() {

        Command nullCommand = Mockito.mock(Command.class);

        when(nullCommand.execute(userThreadMock)).
                thenReturn(null);

        ConsoleMenu consoleMenu = new ConsoleMenu(nullCommand);
        consoleMenu.run(userThreadMock);
        Assert.assertNull(nullCommand.execute(userThreadMock));
    }
}
