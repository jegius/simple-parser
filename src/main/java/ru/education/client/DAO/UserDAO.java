package ru.education.client.DAO;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.DAO;

import ru.education.client.parser.model.User;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.utils.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@DAO
public class UserDAO {
    @Bind
    private JDBCUtil jdbcUtil;

    @Bind
    private DataFormatUtil dataFormatUtil;

    public User addUser(User user) throws SQLException {
        try (Connection connection = jdbcUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO Usr\n" +
                             " (name,\n" +
                             " last_name,\n" +
                             " password,\n" +
                             " photo,\n" +
                             " email,\n" +
                             " registration_date, \n" +
                             " is_active, \n" +
                             " login)\n" +
                             "VALUES (?, ?, ?, ?, ?, current_timestamp, ? , ?)\n" +
                             "RETURNING id, registration_date")) {

            statement.setString(1, user.getName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getPhoto());
            statement.setString(5, user.getEmail());
            statement.setBoolean(6, user.isActive());
            statement.setString(7, user.getLogin());
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            if (resultSet.next()) {
                user.setId((UUID) resultSet.getObject(1));
                user.setRegistrationDate(resultSet.getDate(2));
                return user;
            }
        } catch (SQLException throwables) {
            throw new SQLException();
        }
        return null;
    }

    public User getUserByLogin(String login) {
        try (Connection connection = jdbcUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT\n" +
                             "  id,\n" +
                             "  role_id,\n" +
                             "  name,\n" +
                             "  last_name,\n" +
                             "  password,\n" +
                             "  photo,\n" +
                             "  email,\n" +
                             "  registration_date,\n" +
                             "  is_active,\n" +
                             " login\n" +
                             "FROM usr\n" +
                             "WHERE login = ?")) {

            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {

                return User.userBuilder()
                        .setId((UUID) resultSet.getObject("id"))
                        .setRoleId((UUID) resultSet.getObject("role_id"))
                        .setName(resultSet.getString("name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setPassword(resultSet.getString("password"))
                        .setPhoto(resultSet.getString("photo"))
                        .setEmail(resultSet.getString("email"))
                        .setRegistrationDate(resultSet.getDate("registration_date"))
                        .setIsActive(resultSet.getBoolean("is_active"))
                        .setLogin(resultSet.getString("login"))
                        .build();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }


    public User updateUser(User user) {
        try (Connection connection = jdbcUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "update usr set " +
                             "name = ?, " +
                             "login = ?, " +
                             "password = ?, " +
                             "last_name = ?, " +
                             "email = ?, " +
                             "photo = ?, " +
                             "is_active = ? " +
                             "where id = ?")) {

            statement.setString(1, user.getName());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getPhoto());
            statement.setBoolean(7, user.isActive());
            statement.setObject(8, user.getId());
            statement.execute();

            return user;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }

    }
}
