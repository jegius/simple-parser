package ru.education.client.DAO;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.DAO;
import ru.education.client.parser.model.Role;
import ru.education.client.parser.model.User;
import ru.education.client.utils.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@DAO
public class RoleDAO {
    @Bind
    private JDBCUtil jdbcUtil;

    public Role addRole(Role role) {
        try (Connection connection = jdbcUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO Role\n" +
                             "( role_name\n" +
                             " )\n" +
                             "VALUES (?)\n" +
                             "RETURNING id")) {

            statement.setString(1, role.getRoleName());
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            if (resultSet.next()) {
                role.setId((UUID) resultSet.getObject(1));
                return role;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return role;
    }

    public Role getRoleByName(String roleName) {
        try (Connection connection = jdbcUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT\n" +
                             "  id,\n" +
                             "  role_name\n" +
                             "FROM role\n" +
                             "WHERE role_name = ?")) {

            statement.setString(1, roleName);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {

                return Role.roleBuilder()
                        .setId((UUID) resultSet.getObject("id"))
                        .setRoleName(resultSet.getString("role_name"))
                        .build();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public List<Role> getAllRoles() {
        List<Role> roles = new ArrayList<>();
        try (Connection connection = jdbcUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT\n" +
                             "  *" +
                             "FROM role\n"
             )) {

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                roles.add(Role.roleBuilder()
                        .setId((UUID) resultSet.getObject("id"))
                        .setRoleName(resultSet.getString("role_name"))
                        .build());
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return roles;
    }

    public Role updateRole(Role role) {
        try (Connection connection = jdbcUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "update role set role_name = ? where id = ?")) {

            statement.setString(1, role.getRoleName());
            statement.setObject(2, role.getId());
            statement.execute();

            return role;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }

    }
}
