package ru.education.client.DAO;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.DAO;
import ru.education.client.parser.model.Role;
import ru.education.client.parser.model.Setting;
import ru.education.client.utils.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@DAO
public class SettingDAO {
    @Bind
    private JDBCUtil jdbcUtil;

    public Setting addSetting(Setting setting) {
        try (Connection connection = jdbcUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO setting\n" +
                             "(setting_key, \n" +
                             " setting_value)\n" +
                             "VALUES (?, ?)\n" +
                             "RETURNING id")) {

            statement.setString(1, setting.getKey());
            statement.setString(2, setting.getValue());
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            if (resultSet.next()) {
                setting.setId((UUID) resultSet.getObject("id"));
                return setting;
            }
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
            throwables.printStackTrace();
        }
        return setting;
    }

    public List<Setting> getAllSettings() {
        List<Setting> settings = new ArrayList<>();
        try (Connection connection = jdbcUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT\n" +
                             "  *" +
                             "FROM setting\n"
             )) {

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                settings.add(Setting.optionBuilder()
                        .setId((UUID) resultSet.getObject("id"))
                        .setKey(resultSet.getString("setting_key"))
                        .setValue(resultSet.getString("setting_value"))
                        .build()
                );
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return settings;
    }


}
