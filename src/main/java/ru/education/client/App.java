package ru.education.client;

import ru.education.client.consts.CommandsList;
import ru.education.client.menu.ConsoleMenu;

import ru.education.client.services.CommandManager;

public class App {
    public static void main(String[] args) {
        run();
    }

    public static void run() {
        ConsoleMenu menu = new ConsoleMenu(
                CommandManager
                        .getCommandByChoice(CommandsList.START_MENU)
        );
        menu.run(null);
    }
}
