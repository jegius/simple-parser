package ru.education.client.annotations;

import ru.education.client.consts.CommandsList;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface CommandMark {
    CommandsList command();
}
