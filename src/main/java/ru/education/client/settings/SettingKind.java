package ru.education.client.settings;

public enum SettingKind {
    SETTING_FROM_REPO,
    SETTING_FROM_CONFIG;

    public String getKind() {
        return kind;
    }

    private String kind;
}
