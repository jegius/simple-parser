package ru.education.client.settings;

import ru.education.client.parser.model.IdentifiedItem;
import ru.education.client.parser.model.Setting;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

public class SettingFactory {

    private static SettingFactory instance;

    private SettingFactory() {
    }

    public static synchronized SettingFactory getInstance() {
        if (instance == null) {
            instance = new SettingFactory();
        }
        return instance;
    }

    private final BiFunction<List<Setting>, List<Setting>, List<Setting>> filterSettings = (
            listSettingOne,
            listSettingTwo
    ) ->
    {
        if (listSettingOne.isEmpty() && !listSettingTwo.isEmpty()) {
            return listSettingTwo;
        } else if (!listSettingOne.isEmpty() && listSettingTwo.isEmpty()) {
            return listSettingOne;
        } else if (listSettingOne.isEmpty() && listSettingTwo.isEmpty()) {
            return new ArrayList<>();
        }

        BiFunction<List<Setting>, Setting, String> findTheSameSetting = (list, setting) -> {
            Optional<Setting> filterResult = list
                    .stream()
                    .filter(confSetting -> confSetting.getId().equals(setting.getId()))
                    .findFirst();
            return filterResult.map(Setting::getKey).orElse(null);
        };
        return listSettingOne
                .stream()
                .filter(setting -> setting.getId().equals(findTheSameSetting.apply(listSettingTwo, setting)))
                .collect(Collectors.toList());
    };

    private final BiFunction<List<Setting>, List<Setting>, List<Setting>> mergeRepoPriorityStrategy = (
            settingsFromRepo,
            settingsFromConfig
    ) -> filterSettings.apply(settingsFromRepo, settingsFromConfig);

    private final BiFunction<List<Setting>, List<Setting>, List<Setting>> mergeConfigPriorityStrategy = (
            settingsFromRepo,
            settingsFromConfig
    ) -> filterSettings.apply(settingsFromConfig, settingsFromRepo);

    public BiFunction<List<Setting>, List<Setting>, List<Setting>> getMergedStrategy(SettingKind kind) {
        switch (kind) {
            case SETTING_FROM_REPO:
                return mergeRepoPriorityStrategy;
            case SETTING_FROM_CONFIG:
                return mergeConfigPriorityStrategy;
            default:
                return null;
        }
    }
}
