package ru.education.client.settings;

import ru.education.client.Configuration;
import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Service;
import ru.education.client.parser.model.Setting;
import ru.education.client.repository.SettingRepository;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.services.OnInitService;
import ru.education.client.utils.RegExp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SettingsService extends OnInitService {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private SettingRepository settingRepository;

    private List<Setting> settings;

    public SettingsService() {
    }

    @Override
    public void init() {
        settings = getMergedSettings(getSettingsFromProperties());
    }

    private List<Setting> getSettingsFromProperties() {
        return Configuration
                .getPropertiesKeys()
                .stream()
                .filter(key -> key.matches(RegExp.SETTINGS.getValue()))
                .map(key -> new Setting(UUID.randomUUID(), key, Configuration.getProperty(key)))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    private List<Setting> getMergedSettings(List<Setting> settingsFromConfig) {
        SettingKind mergeStrategy;
        try {
            mergeStrategy = SettingKind.valueOf(Configuration.getProperty("merge-strategy.setting"));
        } catch (IllegalArgumentException illegalArgumentException) {
            dataFormatUtil.printTranslatedMessage("settingService.wrongSetting.warning");
            mergeStrategy = SettingKind.SETTING_FROM_REPO;
        }

        List<Setting> commonSettings = SettingFactory
                .getInstance()
                .getMergedStrategy(mergeStrategy)
                .apply(settingRepository.get(), settingsFromConfig);

        if (!settingRepository.get().isEmpty()) {
            commonSettings.forEach(setting -> settingRepository.remove(setting));
        }
        commonSettings.forEach(setting -> settingRepository.add(setting));

        return settingRepository.get();
    }

    public String getSettingValue(String key) {

        Optional<Setting> setting = settings
                .stream()
                .filter(currentSetting -> currentSetting.getKey().equals(key))
                .findFirst();

        return setting
                .map(Setting::getValue)
                .orElse(null);
    }
}
