package ru.education.client.parser.model;

import java.util.UUID;

public class Role extends IdentifiedItem {

    private String roleName;

    private Role() {
    }

    public Role(UUID id,
                String roleName) {
        super(id);
        this.roleName = roleName;
    }

    public Role(Role role) {
        this(
                role.getId(),
                role.getRoleName()
        );
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public static RoleBuilder roleBuilder() {
        return new Role().new RoleBuilder();
    }

    @Override
    public String toString() {
        return "Role{" +
                "Id = '" + super.getId() + '\'' +
                "Name='" + roleName + '\'' +
                '}';
    }

    public class RoleBuilder {
        private RoleBuilder() {
        }

        public RoleBuilder setId(UUID id) {
            Role.super.setId(id);
            return this;
        }

        public RoleBuilder setRoleName(String roleName) {
            Role.this.roleName = roleName;
            return this;
        }

        public Role build() {
            return Role.this;
        }
    }
}
