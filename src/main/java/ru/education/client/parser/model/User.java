package ru.education.client.parser.model;

import java.sql.Date;
import java.util.Objects;
import java.util.UUID;

public class User extends IdentifiedItem {

    private UUID roleId;
    private String name;
    private String login;
    private String password;
    private String lastName;
    private String email;
    private String photo;



    private Date registrationDate;
    private boolean isActive;

    public User() {
    }

    public User(UUID id,
                UUID roleId,
                String name,
                String login,
                String password,
                String lastName,
                String email,
                String photo,
                Date registrationDate,
                boolean isActive) {
        super(id);
        this.roleId = roleId;
        this.name = name;
        this.login = login;
        this.password = password;
        this.lastName = lastName;
        this.email = email;
        this.photo = photo;
        this.isActive = isActive;
        this.registrationDate =registrationDate;
    }

    public User(User user) {
        this(
                user.getId(),
                user.getRoleId(),
                user.getName(),
                user.getLogin(),
                user.getPassword(),
                user.getLastName(),
                user.getEmail(),
                user.getPhoto(),
                user.getRegistrationDate(),
                user.isActive()
        );
    }

    public UUID getRoleId() {
        return roleId;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setRoleId(UUID roleId) {
        this.roleId = roleId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public static UserBuilder userBuilder() {
        return new User().new UserBuilder();
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "roleId='" + roleId + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public class UserBuilder {
        private UserBuilder() {
        }

        public UserBuilder setId(UUID id) {
            User.super.setId(id);
            return this;
        }

        public UserBuilder setRoleId(UUID roleId) {
            User.this.setRoleId(roleId);
            return this;
        }

        public UserBuilder setName(String name) {
            User.this.setName(name);
            return this;
        }

        public UserBuilder setLogin(String login) {
            User.this.setLogin(login);
            return this;
        }

        public UserBuilder setPassword(String password) {
            User.this.setPassword(password);
            return this;
        }

        public UserBuilder setLastName(String lastName) {
            User.this.setLastName(lastName);
            return this;
        }

        public UserBuilder setEmail(String email) {
            User.this.setEmail(email);
            return this;
        }

        public UserBuilder setPhoto(String photo) {
            User.this.setPhoto(photo);
            return this;
        }

        public UserBuilder setIsActive(boolean active) {
            User.this.setActive(active);
            return this;
        }

        public UserBuilder setRegistrationDate(Date date){
            User.this.setRegistrationDate(date);
            return this;
        }
        public User build() {
            return User.this;
        }

    }
}
