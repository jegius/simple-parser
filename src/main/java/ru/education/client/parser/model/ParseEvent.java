package ru.education.client.parser.model;

import java.util.UUID;

public class ParseEvent extends IdentifiedItem {
    private String date;
    private String dailyParseTime;
    private boolean isSuccess;

    private ParseEvent() {}

    public ParseEvent(
            UUID id,
            String date,
            String dailyParseTime,
            boolean isSuccess
    ) {
        super(id);
        this.date = date;
        this.dailyParseTime = dailyParseTime;
        this.isSuccess = isSuccess;
    }

    public ParseEvent(ParseEvent parseEvent) {
        this(parseEvent.getId(),
                parseEvent.getDate(),
                parseEvent.getDailyParseTime(),
                parseEvent.getSuccess());
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDailyParseTime() {
        return dailyParseTime;
    }

    public void setDailyParseTime(String dailyParseTime) {
        this.dailyParseTime = dailyParseTime;
    }

    public boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    @Override
    public String toString() {
        return "Parse Event: " +
                "ID - " + super.getId() +
                ", date - " + this.date +
                ", daily parse time - " + this.dailyParseTime;
    }

    public static ParseEventBuilder parseEventBuilder() {
        return new ParseEvent().new ParseEventBuilder();
    }

    public class ParseEventBuilder {
        private ParseEventBuilder() {}

        public ParseEventBuilder setId(UUID id) {
            ParseEvent.this.setId(id);
            return this;
        }

        public ParseEventBuilder setDate(String date) {
            ParseEvent.this.date = date;
            return this;
        }

        public ParseEventBuilder setDailyParseTime(String dailyParseTime) {
            ParseEvent.this.dailyParseTime = dailyParseTime;
            return this;
        }

        public ParseEventBuilder setSuccess(boolean isSuccess) {
            ParseEvent.this.isSuccess = isSuccess;
            return this;
        }

        public ParseEvent build() {
            return ParseEvent.this;
        }
    }
}
