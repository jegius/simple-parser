package ru.education.client.parser.model;

import java.util.UUID;

public class WebSite extends IdentifiedItem {
    private String title;
    private String baseUrl;
    private boolean isActive;
    private String description;
    private String coverUrl;

    private WebSite() {}

    public WebSite(
            UUID id,
            String title,
            String baseUrl,
            boolean isActive,
            String description,
            String coverUrl
    ) {
        super(id);
        this.title = title;
        this.baseUrl = baseUrl;
        this.isActive = isActive;
        this.description = description;
        this.coverUrl = coverUrl;
    }

    public WebSite(WebSite object) {
        this(object.getId(),
                object.getTitle(),
                object.getBaseUrl(),
                object.getActive(),
                object.getDescription(),
                object.getCoverUrl());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public boolean getActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    @Override
    public String toString() {
        return "Website: " +
                "ID - " + super.getId() +
                ", title - " + this.title +
                ", base URL - " + this.baseUrl +
                ", description - " + this.description;
    }

    public static WebsiteBuilder websiteBuilder() {
        return new WebSite().new WebsiteBuilder();
    }

    public class WebsiteBuilder {
        private WebsiteBuilder(){}

        public WebsiteBuilder setId(UUID id) {
            WebSite.this.setId(id);
            return this;
        }

        public WebsiteBuilder setTitle (String title) {
            WebSite.this.title = title;
            return this;
        }

        public WebsiteBuilder setBaseUrl(String baseUrl) {
            WebSite.this.baseUrl = baseUrl;
            return this;
        }

        public WebsiteBuilder setActive(boolean isActive) {
            WebSite.this.isActive = isActive;
            return this;
        }

        public WebsiteBuilder setDescription(String description) {
            WebSite.this.description = description;
            return this;
        }

        public WebsiteBuilder setCoverUrl(String coverUrl) {
            WebSite.this.coverUrl = coverUrl;
            return this;
        }

        public WebSite build() {
            return WebSite.this;
        }
    }
}