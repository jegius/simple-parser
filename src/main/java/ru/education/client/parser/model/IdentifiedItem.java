package ru.education.client.parser.model;

import java.util.UUID;

public abstract class IdentifiedItem {

    private UUID id;

    public IdentifiedItem() {
    }

    public IdentifiedItem(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}