package ru.education.client.parser.model;

import java.util.UUID;

public class Setting extends IdentifiedItem {
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private String key;

    public Setting(UUID id, String key, String value) {
        super(id);
        this.key = key;
        this.value = value;
    }

    private Setting() {
    }

    public Setting(Setting option) {
        this(
                option.getId(),
                option.getKey(),
                option.getValue()
        );
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static OptionBuilder optionBuilder() {
        return new Setting().new OptionBuilder();
    }

    public class OptionBuilder {
        private OptionBuilder() {

        }

        public OptionBuilder setId(UUID id) {
            Setting.super.setId(id);
            return this;
        }

        public OptionBuilder setValue(String value) {
            Setting.this.setValue(value);
            return this;
        }

        public OptionBuilder setKey(String key) {
            Setting.this.setKey(key);
            return this;
        }

        public Setting build() {
            return Setting.this;
        }
    }

}
