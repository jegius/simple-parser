package ru.education.client.parser.model;

import java.util.UUID;

public class Target extends IdentifiedItem {
    private UUID parseEventId;
    private String secondUrl;
    private String name;
    private String htmlTag;
    private String description;
    private String result;


    private Target() {}

    public Target(
            UUID id,
            UUID parseEventId,
            String secondUrl,
            String name,
            String htmlTag,
            String description,
            String result
    ) {
        super(id);
        this.parseEventId = parseEventId;
        this.secondUrl = secondUrl;
        this.name = name;
        this.htmlTag = htmlTag;
        this.description = description;
        this.result = result;
    }

    public Target(Target target) {
        this(target.getId(),
                target.getParseEventId(),
                target.getSecondUrl(),
                target.getName(),
                target.getHtmlTag(),
                target.getDescription(),
                target.getResult());
    }

    public UUID getParseEventId() {
        return parseEventId;
    }

    public String getSecondUrl() {
        return secondUrl;
    }

    public String getName() {
        return name;
    }

    public String getHtmlTag() {
        return htmlTag;
    }

    public String getResult() {
        return result;
    }

    public String getDescription() {
        return description;
    }

    public void setParseEventId(UUID parseEventId) {
        this.parseEventId = parseEventId;
    }

    public void setSecondUrl(String secondUrl) {
        this.secondUrl = secondUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHtmlTag(String htmlTag) {
        this.htmlTag = htmlTag;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Target: " +
                "ID - " + super.getId() +
                ", Parse Event ID - " + parseEventId +
                ", second URL - " + this.secondUrl +
                ", name - " + this.name +
                ", HTML Tag - " + this.htmlTag +
                ", result - " + this.result +
                ", description - " + this.description;
    }

    public static TargetBuilder targetBuilder() {
        return new Target().new TargetBuilder();
    }

    public class TargetBuilder {
        private TargetBuilder() {}

        public TargetBuilder setId(UUID id) {
            Target.this.setId(id);
            return this;
        }

        public TargetBuilder setParseEventId(UUID parseEventId) {
            Target.this.setParseEventId(parseEventId);
            return this;
        }

        public TargetBuilder setSecondUrl(String secondUrl) {
            Target.this.secondUrl = secondUrl;
            return this;
        }

        public TargetBuilder setName(String name) {
            Target.this.name = name;
            return this;
        }

        public TargetBuilder setHtmlTag(String htmlTag) {
            Target.this.htmlTag = htmlTag;
            return this;
        }

        public TargetBuilder setResult(String result) {
            Target.this.result = result;
            return this;
        }

        public TargetBuilder setDescription(String description) {
            Target.this.description = description;
            return this;
        }

        public Target build() {
            return Target.this;
        }
    }
}
