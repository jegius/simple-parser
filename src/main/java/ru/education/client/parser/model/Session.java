package ru.education.client.parser.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Session extends IdentifiedItem {

    private Map<String, Object> data;

    private Date expirationDate;

    public Session() {
        data = new HashMap<>();
    }

    public Session(UUID id,
                   Map<String, Object> data,
                   Date expirationDate) {
        super(id);
        this.data = data;
        this.expirationDate = expirationDate;
    }

    public Session(Session session) {
        this(
                session.getId(),
                session.getData(),
                session.getExpirationDate()
        );
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public static Session.SessionBuilder sessionBuilder() {
        return new Session().new SessionBuilder();
    }

    public class SessionBuilder {
        private SessionBuilder() {
        }

        public Session.SessionBuilder setId(UUID id) {
            Session.super.setId(id);
            return this;
        }

        public Session.SessionBuilder setData(String key, Object object) {
            Session.this.data.put(key, object);
            return this;
        }

        public Session.SessionBuilder setExpirationDate(Date expirationDate) {
            Session.this.setExpirationDate(expirationDate);
            return this;
        }

        public Session build() {
            return Session.this;
        }
    }
}
