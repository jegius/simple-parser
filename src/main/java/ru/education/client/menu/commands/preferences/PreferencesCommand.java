package ru.education.client.menu.commands.preferences;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.PREFERENCES)
public class PreferencesCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public PreferencesCommand() {
    }

    @Override
    public Command execute() {

        CommandsList[] commandsOptions = {
                CommandsList.MAIN_MENU,
                CommandsList.LANGUAGE,
                CommandsList.CHANGE_PASSWORD,
                CommandsList.CHANGE_PHOTO
        };

        dataFormatUtil.printOptions(commandsOptions);
        CommandsList choice = dataFormatUtil.selectCommand(commandsOptions);

        return CommandManager
                .getCommandByChoice(choice);
    }
}
