package ru.education.client.menu.commands.website.CRUD.utils;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Util;
import ru.education.client.parser.model.WebSite;
import ru.education.client.repository.WebSiteRepository;
import ru.education.client.utils.DoCustomAction;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.utils.ValidationUtils;

@Util
public class WebsiteCommandUtils {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private ValidationUtils validationUtils;

    @Bind
    private WebSiteRepository webSiteRepository;

    public DoCustomAction<WebSite> deleteWebsiteCustomAction = webSite -> {
        webSiteRepository.remove(webSite);

        dataFormatUtil.printDivider();
        dataFormatUtil.printTranslatedMessage(
                "website.deleteWebsite.success",
                new String[]{webSite.getTitle()});
        dataFormatUtil.waitForEnter();
    };

    public  DoCustomAction<WebSite> updateWebsiteCustomAction = webSite -> {

        webSite.setTitle(dataFormatUtil.getUserInput(
                "website.createWebsite.enterTitle",
                validationUtils.emptyValidation));
        webSite.setBaseUrl(dataFormatUtil.getUserInput(
                "website.createWebsite.enterBaseUrl",
                validationUtils.urlValidation));
        webSite.setDescription(dataFormatUtil.getUserInput(
                "website.createWebsite.enterDescription",
                validationUtils.emptyValidation));
        webSite.setCoverUrl(dataFormatUtil.getUserInput(
                "website.createWebsite.enterCoverUrl",
                validationUtils.urlValidation));

        webSiteRepository.update(webSite);

        dataFormatUtil.printDivider();

        dataFormatUtil.printTranslatedMessage(
                "website.updateWebsite.success",
                new String[]{webSite.getTitle()});
    };
}
