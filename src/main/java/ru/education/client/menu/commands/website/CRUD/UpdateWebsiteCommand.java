package ru.education.client.menu.commands.website.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.website.CRUD.utils.WebsiteCommandUtils;
import ru.education.client.repository.WebSiteRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.UPDATE_WEBSITE)
public class UpdateWebsiteCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private WebsiteCommandUtils websiteCommandUtils;

    @Bind
    private WebSiteRepository webSiteRepository;

    public UpdateWebsiteCommand() {
    }

    @Override
    public Command execute() {

        dataFormatUtil.printListItem(webSiteRepository.get());

        return dataFormatUtil
                .checkItemExistingAndDoCustom(
                        CommandManager
                                .getCommandByChoice(CommandsList.WEBSITE_REPOSITORY),
                        websiteCommandUtils.updateWebsiteCustomAction,
                        webSiteRepository
                );
    }
}
