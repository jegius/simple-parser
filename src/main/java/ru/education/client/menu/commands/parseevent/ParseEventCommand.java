package ru.education.client.menu.commands.parseevent;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.PARSE_EVENT_REPOSITORY)
public class ParseEventCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public ParseEventCommand() {
    }

    @Override
    public Command execute() {

        CommandsList[] commandsOptions = {
                CommandsList.MAIN_MENU,
                CommandsList.CREATE_PARSE_EVENT,
                CommandsList.DELETE_PARSE_EVENT,
                CommandsList.UPDATE_PARSE_EVENT,
                CommandsList.READ_PARSE_EVENT,
                CommandsList.SAVE_PARSE_EVENT,
        };

        dataFormatUtil.printOptions(commandsOptions);
        CommandsList choice = dataFormatUtil.selectCommand(commandsOptions);

        return CommandManager
                .getCommandByChoice(choice);
    }
}
