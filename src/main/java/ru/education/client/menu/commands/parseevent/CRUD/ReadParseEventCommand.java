package ru.education.client.menu.commands.parseevent.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.parser.model.ParseEvent;
import ru.education.client.repository.ParseEventRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@CommandMark(command = CommandsList.READ_PARSE_EVENT)
public class ReadParseEventCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private ParseEventRepository parseEventRepository;

    public ReadParseEventCommand() {
    }

    @Override
    public Command execute() {
        List<ParseEvent> parseEvents = parseEventRepository.get();

        if (parseEvents.isEmpty()) {
            dataFormatUtil.printTranslatedMessage("parseEvent.readParseEvent.emptyWarning");
        } else {
            dataFormatUtil.printListItem(parseEvents);
        }

        return CommandManager
                .getCommandByChoice(CommandsList.PARSE_EVENT_REPOSITORY);
    }
}
