package ru.education.client.menu.commands.parseevent;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.repository.ParseEventRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.SAVE_PARSE_EVENT)
public class ParseEventSaveCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private ParseEventRepository parseEventRepository;

    public ParseEventSaveCommand() {
    }

    @Override
    public Command execute() {
        parseEventRepository.save();

        dataFormatUtil.printTranslatedMessage("parseEvent.saveParseEvent.success");
        dataFormatUtil.waitForEnter();

        return CommandManager
                .getCommandByChoice(CommandsList.PARSE_EVENT_REPOSITORY);
    }
}
