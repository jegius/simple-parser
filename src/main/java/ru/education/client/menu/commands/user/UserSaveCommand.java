package ru.education.client.menu.commands.user;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.repository.UserRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.SAVE_USER)
public class UserSaveCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private UserRepository userRepository;

    public UserSaveCommand() {
    }

    @Override
    public Command execute() {
        userRepository.save();

        dataFormatUtil.printTranslatedMessage("user.save.success");
        dataFormatUtil.waitForEnter();

        return  CommandManager
                .getCommandByChoice(CommandsList.USER_REPOSITORY);
    }
}
