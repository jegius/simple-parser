package ru.education.client.menu.commands.parseevent.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.parseevent.CRUD.utils.ParseEventCommandUtils;
import ru.education.client.repository.ParseEventRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.DELETE_PARSE_EVENT)
public class DeleteParseEventCommand extends ThreadCommand {

    @Bind
    private ParseEventCommandUtils parseEventCommandUtils;

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private ParseEventRepository parseEventRepository;

    public DeleteParseEventCommand() {
    }

    @Override
    public Command execute() {

        dataFormatUtil.printTranslatedMessage("parseEvent.chooseParseEvent.message");

        dataFormatUtil.printListItem(parseEventRepository.get());

        return
        dataFormatUtil
                .checkItemExistingAndDoCustom(
                        CommandManager
                                .getCommandByChoice(CommandsList.PARSE_EVENT_REPOSITORY),
                        parseEventCommandUtils.deleteParseEventCustomAction,
                        parseEventRepository
                );
    }
}
