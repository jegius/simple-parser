package ru.education.client.menu.commands.language;


import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.consts.Constant;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.services.CommandManager;
import ru.education.client.services.localization.LocalizationService;
import ru.education.client.menu.Command;
import ru.education.client.utils.DataFormatUtil;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

@CommandMark(command = CommandsList.LANGUAGE)
public class LanguageCommand extends ThreadCommand {

    @Bind
    private LocalizationService localizationService;

    @Bind
    private DataFormatUtil dataFormatUtil;

    public LanguageCommand() {
    }

    @Override
    public Command execute() {
        return selectLanguage();
    }

    private int getUserChoice(List<String> languages) {
        Scanner scanner = dataFormatUtil.getScanner();
        int choice;

        try {
            choice = scanner.nextInt();
            if (
                    choice < Integer.parseInt(Constant.FIRST_ELEMENT_OF_LIST.getValue())
                            || choice > languages.size()
            ) {
                throw new IndexOutOfBoundsException();
            }
        } catch (InputMismatchException | IndexOutOfBoundsException e) {
            dataFormatUtil.printTranslatedMessage("menu.command.wrongChoice");
            return getUserChoice(languages);
        }
        return choice;
    }

    private Command selectLanguage() {

        List<String> languages = localizationService
                .getAvailableLocales();

        dataFormatUtil.printDivider();

        dataFormatUtil.printListItem(languages);

        int choice = getUserChoice(languages);

        localizationService
                .setCurrentLocalization(languages
                        .get(choice - Integer.parseInt(Constant.FIRST_ELEMENT_OF_LIST.getValue())));

        return CommandManager
                .getCommandByChoice(CommandsList.MAIN_MENU);
    }
}
