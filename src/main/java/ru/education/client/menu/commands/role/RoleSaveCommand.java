package ru.education.client.menu.commands.role;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.repository.RoleRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.SAVE_ROLE)
public class RoleSaveCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private RoleRepository roleRepository;

    public RoleSaveCommand() {
    }

    @Override
    public Command execute() {
        roleRepository.save();

        dataFormatUtil.printTranslatedMessage("role.saveRole.success");

        dataFormatUtil.waitForEnter();

        return CommandManager
                .getCommandByChoice(CommandsList.ROLE_REPOSITORY);
    }
}
