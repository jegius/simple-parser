package ru.education.client.menu.commands.auth;

import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;

@CommandMark(command = CommandsList.LOG_OUT)
public class LogOutCommand extends ThreadCommand {

    public LogOutCommand() {
    }

    @Override
    public Command execute() {
        return null;
    }
}
