package ru.education.client.menu.commands.parseevent.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.parseevent.CRUD.utils.ParseEventCommandUtils;
import ru.education.client.parser.model.ParseEvent;
import ru.education.client.repository.ParseEventRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.CREATE_PARSE_EVENT)
public class CreateParseEventCommand extends ThreadCommand {

    @Bind
    private ParseEventCommandUtils parseEventCommandUtils;

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private ParseEventRepository parseEventRepository;

    public CreateParseEventCommand() {
    }

    @Override
    public Command execute() {

        ParseEvent parseEvent = ParseEvent.parseEventBuilder()
                .setDate(dataFormatUtil.getUserInput(
                        "parseEvent.createParseEvent.enterDate",
                        parseEventCommandUtils.dateValidation))
                .setDailyParseTime(dataFormatUtil.getUserInput(
                        "parseEvent.createParseEvent.enterDailyParseTime",
                        parseEventCommandUtils.dateValidation))
                .build();

        parseEventRepository.add(parseEvent);

        return CommandManager
                .getCommandByChoice(CommandsList.PARSE_EVENT_REPOSITORY);
    }
}
