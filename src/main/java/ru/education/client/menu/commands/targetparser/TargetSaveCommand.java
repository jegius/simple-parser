package ru.education.client.menu.commands.targetparser;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.repository.TargetRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.SAVE_TARGET)
public class TargetSaveCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private TargetRepository targetRepository;

    public TargetSaveCommand() {
    }

    @Override
    public Command execute() {
        targetRepository.save();

        dataFormatUtil.printTranslatedMessage("target.saveTarget.success");
        dataFormatUtil.waitForEnter();

        return CommandManager
                .getCommandByChoice(CommandsList.TARGET_REPOSITORY);
    }
}
