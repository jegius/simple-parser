package ru.education.client.menu.commands.auth;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.user.CRUD.utils.UserCommandUtils;
import ru.education.client.parser.model.User;
import ru.education.client.repository.UserRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

import java.sql.SQLException;

@CommandMark(command = CommandsList.REGISTRATION)
public class RegistrationMenuCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private UserCommandUtils userCommandUtils;

    @Bind
    private UserRepository userRepository;

    public RegistrationMenuCommand() {
    }

    @Override
    public Command execute() {

        User user = userCommandUtils.createUser();

        try {
            userRepository.add(user);
        } catch (SQLException e) {
            dataFormatUtil.printTranslatedMessage("user.utilsValidation.loginWarning");
            this.execute();
        }

        userRepository.save();

        dataFormatUtil.printTranslatedMessage("user.createUser.success", new String[]{user.getName()});

        return CommandManager
                .getCommandByChoice(CommandsList.START_MENU);
    }
}
