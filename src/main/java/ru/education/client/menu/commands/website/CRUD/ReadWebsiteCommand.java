package ru.education.client.menu.commands.website.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.parser.model.WebSite;
import ru.education.client.repository.WebSiteRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@CommandMark(command = CommandsList.READ_WEBSITE)
public class ReadWebsiteCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private WebSiteRepository webSiteRepository;

    public ReadWebsiteCommand() {
    }

    @Override
    public Command execute() {
        List<WebSite> webSites = webSiteRepository.get();

        if (webSites.isEmpty()) {
            dataFormatUtil.printTranslatedMessage("website.readWebsite.emptyWarning");
        } else {
            dataFormatUtil.printListItem(webSites);
        }

        return CommandManager
                .getCommandByChoice(CommandsList.WEBSITE_REPOSITORY);
    }
}