package ru.education.client.menu.commands.targetparser.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.parser.model.Target;
import ru.education.client.repository.TargetRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@CommandMark(command = CommandsList.READ_TARGET)
public class ReadTargetCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private TargetRepository targetRepository;

    public ReadTargetCommand() {
    }

    @Override
    public Command execute() {
        List<Target> targets = targetRepository.get();

        if (targets.isEmpty()) {
            dataFormatUtil.printTranslatedMessage("target.readTarget.emptyWarning");
        } else {
            dataFormatUtil.printListItem(targets);
        }

        return CommandManager
                .getCommandByChoice(CommandsList.TARGET_REPOSITORY);
    }
}
