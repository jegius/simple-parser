package ru.education.client.menu.commands.role.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.parser.model.Role;
import ru.education.client.repository.RoleRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

import java.util.Objects;
@CommandMark(command = CommandsList.CREATE_ROLE)
public class CreateRoleCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private RoleRepository roleRepository;

    public CreateRoleCommand() {
    }

    @Override
    public Command execute() {

        Role role = Role
                .roleBuilder()
                .setRoleName(dataFormatUtil.getUserInput("role.createRole.enterName", Objects::nonNull))
                .build();

        roleRepository.add(role);

        return CommandManager
                .getCommandByChoice(CommandsList.ROLE_REPOSITORY);
    }
}
