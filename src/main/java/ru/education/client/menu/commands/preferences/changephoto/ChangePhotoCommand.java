package ru.education.client.menu.commands.preferences.changephoto;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.user.CRUD.utils.UserCommandUtils;
import ru.education.client.parser.model.User;
import ru.education.client.repository.SessionRepository;
import ru.education.client.repository.UserRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.services.session.SessionAccessKeys;
import ru.education.client.utils.DataFormatUtil;

import java.util.UUID;

@CommandMark(command = CommandsList.CHANGE_PHOTO)
public class ChangePhotoCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private UserCommandUtils userCommandUtils;

    @Bind
    private SessionRepository sessionRepository;

    @Bind
    private UserRepository userRepository;

    public ChangePhotoCommand() {}

    @Override
    public Command execute() {
        UUID sessionId = this.currentThread.getSessionId();

        User currentUser = (User) sessionRepository
                .getById(sessionId)
                .getData()
                .get(SessionAccessKeys.USER.getValue());

        String newPhoto = dataFormatUtil
                .getUserInput(
                        "user.createUser.photo",
                        userCommandUtils.picValidation
                );

        currentUser.setPhoto(newPhoto);

        userRepository.update(currentUser);
        userRepository.save();

        return CommandManager
                .getCommandByChoice(CommandsList.PREFERENCES);
    }
}
