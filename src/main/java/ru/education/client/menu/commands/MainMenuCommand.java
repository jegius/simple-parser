package ru.education.client.menu.commands;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.MAIN_MENU)
public class MainMenuCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public MainMenuCommand() {
    }

    @Override
    public Command execute() {

        CommandsList[] commandsOptions = {
                CommandsList.WEBSITE_REPOSITORY,
                CommandsList.PARSE_EVENT_REPOSITORY,
                CommandsList.TARGET_REPOSITORY,
                CommandsList.ROLE_REPOSITORY,
                CommandsList.USER_REPOSITORY,
                CommandsList.PREFERENCES,
                CommandsList.LOG_OUT,
        };

        dataFormatUtil.printOptions(commandsOptions);
        CommandsList choice = dataFormatUtil.selectCommand(commandsOptions);

        return CommandManager
                .getCommandByChoice(choice);
    }
}
