package ru.education.client.menu.commands;

import ru.education.client.menu.Command;
import ru.education.client.threads.user.UserThread;

public abstract class ThreadCommand implements Command  {

    protected UserThread currentThread;

    public abstract Command execute();

    public synchronized Command execute(UserThread thread) {
        currentThread = thread;
        return execute();
    }
}
