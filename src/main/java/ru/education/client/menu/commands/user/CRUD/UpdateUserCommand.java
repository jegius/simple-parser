package ru.education.client.menu.commands.user.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.user.CRUD.utils.UserCommandUtils;
import ru.education.client.repository.UserRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.UPDATE_USER)
public class UpdateUserCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private UserCommandUtils userCommandUtils;

    @Bind
    private UserRepository userRepository;

    public UpdateUserCommand() {
    }

    @Override
    public Command execute() {

        return dataFormatUtil.checkItemExistingAndDoCustom(
                CommandManager
                        .getCommandByChoice(CommandsList.USER_REPOSITORY),
                userCommandUtils.updateUserCustomAction,
                userRepository
        );
    }
}
