package ru.education.client.menu.commands.targetparser.CRUD.utils;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Util;
import ru.education.client.parser.model.Target;
import ru.education.client.repository.ParseEventRepository;
import ru.education.client.repository.TargetRepository;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.utils.DoCustomAction;
import ru.education.client.utils.Validation;
import ru.education.client.utils.ValidationUtils;

import java.util.UUID;

@Util
public class TargetCommandUtils {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private ValidationUtils validationUtils;

    @Bind
    private ParseEventRepository parseEventRepository;

    @Bind
    private TargetRepository targetRepository;

    public UUID getParseEventIdForTarget() {

        Validation parseEventValidation = userChoice -> {
            try {
                int convertedChoice = Integer.parseInt(userChoice) - 1;
                return convertedChoice >= 0 && convertedChoice < parseEventRepository.get().size();
            } catch (NumberFormatException | IndexOutOfBoundsException e) {
                dataFormatUtil.printTranslatedMessage("target.targetCommandsUtils.message");
                return false;
            }
        };

        dataFormatUtil.printListItem(parseEventRepository.get());
        int userChoice = Integer.parseInt(dataFormatUtil.getUserInput(
                "target.targetCommandsUtils.enterIdParseEvent",
                parseEventValidation));

        return parseEventRepository.get().get(userChoice - 1).getId();
    }

    public DoCustomAction<Target> deleteTargetCustomAction = target -> {
        targetRepository.remove(target);

        dataFormatUtil.printDivider();
        dataFormatUtil.printTranslatedMessage(
                "target.deleteTarget.success",
                new String[]{target.getName()});
        dataFormatUtil.waitForEnter();
    };

    public DoCustomAction<Target> updateTargetCustomAction = target -> {
        Target.targetBuilder()
                .setParseEventId(getParseEventIdForTarget())
                .setName(dataFormatUtil.getUserInput(
                        "target.createTarget.enterName",
                        validationUtils.emptyValidation))
                .setSecondUrl(dataFormatUtil.getUserInput(
                        "target.createTarget.enterUrl",
                        validationUtils.urlValidation))
                .setHtmlTag(dataFormatUtil.getUserInput(
                        "target.createTarget.enterHtmlTag",
                        validationUtils.emptyValidation))
                .setDescription(dataFormatUtil.getUserInput(
                        "target.createTarget.enterDescription",
                        validationUtils.emptyValidation))
                .build();

        targetRepository.update(target);
    };
}
