package ru.education.client.menu.commands.role.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.role.CRUD.utils.RoleCommandUtils;
import ru.education.client.repository.RoleRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.DELETE_ROlE)
public class DeleteRoleCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private RoleCommandUtils roleCommandUtils;

    @Bind
    private RoleRepository roleRepository;

    public DeleteRoleCommand() {
    }

    @Override
    public Command execute() {

        dataFormatUtil.printListItem(roleRepository.get());

        return dataFormatUtil
                .checkItemExistingAndDoCustom(
                        CommandManager
                                .getCommandByChoice(CommandsList.ROLE_REPOSITORY),
                        roleCommandUtils.deleteRoleCustomAction,
                        roleRepository
                );
    }
}
