package ru.education.client.menu.commands.parseevent.CRUD.utils;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Util;
import ru.education.client.parser.model.ParseEvent;
import ru.education.client.repository.ParseEventRepository;
import ru.education.client.repository.TargetRepository;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.utils.DoCustomAction;
import ru.education.client.utils.Validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Util
public class ParseEventCommandUtils {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private TargetRepository targetRepository;

    @Bind
    private ParseEventRepository parseEventRepository;

    private final String DATE_FORMAT = "dd.MM.yyyy";

    private boolean isValidDate(String inputDate) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);
        dateFormatter.setLenient(false);
        try {
            Date date = dateFormatter.parse(inputDate);
            dateFormatter.format(date);
        } catch (ParseException e) {
            dataFormatUtil.printTranslatedMessage("parseEvent.parseEventCommandUtils.validDate");
            return false;
        }
        return true;
    }

    private void cascadeRemoveTarget(ParseEvent providedParseEvent) {

        boolean isTargets = targetRepository.get()
                .stream().anyMatch(target -> target
                        .getParseEventId()
                        .equals(providedParseEvent.getId())
                );

        if (isTargets) {
            int targetCount = targetRepository.get().size();
            targetRepository.get()
                    .removeIf(target -> target
                            .getParseEventId()
                            .equals(providedParseEvent.getId()));
            dataFormatUtil.printTranslatedMessage(
                    "parseEvent.parseEventCommandUtils.cascadeRemoveMessage",
                    new String[]{String.valueOf(targetCount), providedParseEvent.getDate()});
        }
    }

    public final Validation dateValidation = date -> {
        boolean isEmpty = date.isEmpty();

        if (isEmpty) {
            dataFormatUtil.printTranslatedMessage("parseEvent.parseEventCommandUtils.emptyDate");
        }

        return !isEmpty && isValidDate(date);
    };

    public DoCustomAction<ParseEvent> deleteParseEventCustomAction = parseEvent -> {

        parseEventRepository.remove(parseEvent);

        cascadeRemoveTarget(parseEvent);

        dataFormatUtil.printTranslatedMessage(
                "parseEvent.deleteParseEvent.success",
                new String[]{parseEvent.getDate()});
        dataFormatUtil.waitForEnter();
    };

    public DoCustomAction<ParseEvent> updateParseEventCustomAction = parseEvent -> {

        parseEvent.setDate(dataFormatUtil.getUserInput(
                "parseEvent.createParseEvent.enterDate",
                dateValidation));
        parseEvent.setDailyParseTime(dataFormatUtil.getUserInput(
                "parseEvent.createParseEvent.enterDailyParseTime",
                dateValidation));

        parseEventRepository.update(parseEvent);

        dataFormatUtil.printDivider();

        dataFormatUtil.printTranslatedMessage("parseEvent.updateParseEvent.success");
    };
}
