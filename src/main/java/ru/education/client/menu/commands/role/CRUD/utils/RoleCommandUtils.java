package ru.education.client.menu.commands.role.CRUD.utils;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Util;
import ru.education.client.parser.model.Role;
import ru.education.client.repository.RoleRepository;
import ru.education.client.repository.UserRepository;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.utils.DoCustomAction;

import java.util.Objects;

@Util
public class RoleCommandUtils {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private UserRepository userRepository;

    @Bind
    private RoleRepository roleRepository;

    public  DoCustomAction<Role> deleteRoleCustomAction = role -> {

        boolean isNoUsers = userRepository.get()
                .stream()
                .noneMatch(user -> user
                        .getRoleId()
                        .equals(role.getId())
                );

        if (isNoUsers) {
            roleRepository.remove(role);

            dataFormatUtil.printDivider();

            dataFormatUtil.printTranslatedMessage("role.deleteRole.success", new String[]{role.getRoleName()});

            dataFormatUtil.waitForEnter();

        } else {
            dataFormatUtil.printTranslatedMessage("role.deleteRole.existUserWarning");
        }
    };

    public  DoCustomAction<Role> updateRoleCustomAction = role -> {

        role.setRoleName(dataFormatUtil.getUserInput("role.createRole.enterName", Objects::nonNull));

        roleRepository.update(role);

        dataFormatUtil.printDivider();

        dataFormatUtil.printTranslatedMessage("role.updateRole.success", new String[]{role.getRoleName()});
    };
}

