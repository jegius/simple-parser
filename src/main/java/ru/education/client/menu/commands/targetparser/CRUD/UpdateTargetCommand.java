package ru.education.client.menu.commands.targetparser.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.targetparser.CRUD.utils.TargetCommandUtils;
import ru.education.client.repository.TargetRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.UPDATE_TARGET)
public class UpdateTargetCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private TargetCommandUtils targetCommandUtils;

    @Bind
    private TargetRepository targetRepository;

    public UpdateTargetCommand() {
    }

    @Override
    public Command execute() {
        dataFormatUtil.printListItem(targetRepository.get());

        return dataFormatUtil
                .checkItemExistingAndDoCustom(
                        CommandManager
                                .getCommandByChoice(CommandsList.TARGET_REPOSITORY),
                        targetCommandUtils.updateTargetCustomAction,
                        targetRepository
                );
    }
}
