package ru.education.client.menu.commands.website.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.parser.model.WebSite;
import ru.education.client.repository.WebSiteRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.utils.ValidationUtils;

@CommandMark(command = CommandsList.CREATE_WEBSITE)
public class CreateWebsiteCommand extends ThreadCommand {

    @Bind
    private ValidationUtils validationUtils;

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private WebSiteRepository webSiteRepository;

    public CreateWebsiteCommand() {
    }

    @Override
    public Command execute() {
        WebSite webSite = WebSite
                .websiteBuilder()
                .setTitle(dataFormatUtil.getUserInput(
                        "website.createWebsite.enterTitle",
                        validationUtils.emptyValidation))
                .setBaseUrl(dataFormatUtil.getUserInput(
                        "website.createWebsite.enterBaseUrl",
                        validationUtils.urlValidation))
                .setDescription(dataFormatUtil.getUserInput(
                        "website.createWebsite.enterDescription",
                        validationUtils.emptyValidation))
                .setCoverUrl(dataFormatUtil.getUserInput(
                        "website.createWebsite.enterCoverUrl",
                        validationUtils.urlValidation))
                .build();

        webSiteRepository.add(webSite);

        return CommandManager
                .getCommandByChoice(CommandsList.WEBSITE_REPOSITORY);
    }
}
