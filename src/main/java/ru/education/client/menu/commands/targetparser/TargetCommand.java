package ru.education.client.menu.commands.targetparser;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.TARGET_REPOSITORY)
public class TargetCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public TargetCommand() {
    }

    @Override
    public Command execute() {

        CommandsList[] commandsOptions = {
                CommandsList.MAIN_MENU,
                CommandsList.CREATE_TARGET,
                CommandsList.DELETE_TARGET,
                CommandsList.UPDATE_TARGET,
                CommandsList.READ_TARGET,
                CommandsList.SAVE_TARGET,
        };

        dataFormatUtil.printOptions(commandsOptions);
        CommandsList choice = dataFormatUtil.selectCommand(commandsOptions);

        return CommandManager

                .getCommandByChoice(choice);
    }
}
