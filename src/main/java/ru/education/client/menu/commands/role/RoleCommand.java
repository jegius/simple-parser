package ru.education.client.menu.commands.role;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.ROLE_REPOSITORY)
public class RoleCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public RoleCommand() {
    }

    @Override
    public Command execute() {

        CommandsList[] commandsOptions = {
                CommandsList.MAIN_MENU,
                CommandsList.CREATE_ROLE,
                CommandsList.DELETE_ROlE,
                CommandsList.READ_ROLE,
                CommandsList.SAVE_ROLE,
                CommandsList.UPDATE_ROLE,
        };

        dataFormatUtil.printOptions(commandsOptions);
        CommandsList choice = dataFormatUtil.selectCommand(commandsOptions);

        return CommandManager
                .getCommandByChoice(choice);
    }
}
