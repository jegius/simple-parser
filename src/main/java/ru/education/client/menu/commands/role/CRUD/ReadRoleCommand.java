package ru.education.client.menu.commands.role.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.parser.model.Role;
import ru.education.client.repository.RoleRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@CommandMark(command = CommandsList.READ_ROLE)
public class ReadRoleCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private RoleRepository roleRepository;

    public ReadRoleCommand() {
    }

    @Override
    public Command execute() {

        List<Role> roles = roleRepository.get();

        dataFormatUtil.printDivider();

        if (roles.isEmpty()) {
            dataFormatUtil.printTranslatedMessage("role.readRole.emptyWarning");
        } else {
            dataFormatUtil.printListItem(roles);
        }

        return CommandManager
                .getCommandByChoice(CommandsList.ROLE_REPOSITORY);
    }
}
