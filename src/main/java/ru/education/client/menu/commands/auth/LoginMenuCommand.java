package ru.education.client.menu.commands.auth;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.user.CRUD.utils.UserCommandUtils;
import ru.education.client.passwordinput.PasswordInputMode;
import ru.education.client.repository.UserRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.services.UserService;
import ru.education.client.services.session.ConsoleUserSessionService;
import ru.education.client.threads.user.UserThread;

import java.util.UUID;

@CommandMark(command = CommandsList.LOGIN)
public class LoginMenuCommand extends ThreadCommand {

    @Bind
    private UserService userService;

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private ConsoleUserSessionService consoleUserSessionService;

    @Bind
    private UserCommandUtils userCommandUtils;

    @Bind
    private PasswordInputMode passwordInputMode;

    @Bind
    private UserRepository userRepository;

    @Override
    public Command execute() {
        String login = dataFormatUtil.getUserInput(
                "loginMenu.option.enterLogin",
                userCommandUtils.emptyValueValidation
        );

        String password = passwordInputMode.getPasswordInputByType();

        boolean isAuthRight = userService.checkPassword(login, password);

        if (isAuthRight) {

            UUID sessionId = consoleUserSessionService
                    .initSession(userRepository
                            .getUserByLogin(login));

            UserThread userThread = new UserThread(sessionId);
            userThread.start();

            return CommandManager
                    .getCommandByChoice(CommandsList.EXIT);

        } else {
            dataFormatUtil.printTranslatedMessage("auth.wrongAuth.warning");
            return CommandManager.getCommandByChoice(CommandsList.START_MENU);
        }
    }
}
