package ru.education.client.menu.commands;

import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;

@CommandMark(command = CommandsList.EXIT)
public class ExitCommand extends ThreadCommand {

    public ExitCommand() {
    }

    public Command execute() {
        return null;
    }
}
