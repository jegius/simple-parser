package ru.education.client.menu.commands.user.CRUD.utils;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Util;
import ru.education.client.consts.Constant;
import ru.education.client.parser.model.User;
import ru.education.client.repository.RoleRepository;
import ru.education.client.repository.UserRepository;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.utils.DoCustomAction;
import ru.education.client.utils.RegExp;
import ru.education.client.utils.Validation;

import java.util.UUID;

@Util
public class UserCommandUtils {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private RoleRepository roleRepository;

    @Bind
    private UserRepository userRepository;

    public UUID getRoleID() {

        Validation roleIdValidation = userChoice -> {

            boolean isRoleExist;

            try {
                int choice = Integer.parseInt(userChoice);
                isRoleExist = roleRepository.get().get(choice - 1).getId() != null;
            } catch (NumberFormatException | IndexOutOfBoundsException e) {
                dataFormatUtil.printTranslatedMessage("user.utilsValidation.idWarning");
                isRoleExist = false;
            }
            return isRoleExist;
        };

        dataFormatUtil.printListItem(roleRepository.get());
        int userChoice = Integer.parseInt(dataFormatUtil.getUserInput("user.utils.enterRoleId", roleIdValidation));

        return roleRepository.get().get(userChoice - 1).getId();
    }

     public final Validation emptyValueValidation = name -> {
        boolean isValid = !Constant
                .EMPTY_STRING
                .getValue()
                .equals(name);

        if (!isValid) {
            dataFormatUtil.printTranslatedMessage("user.utilsValidation.emptyValue");
        }

        return isValid;
    };

     public final Validation emailValidation = email -> {
        boolean isValid = email.matches(RegExp.EMAIL.getValue());

        if (!isValid) {
            dataFormatUtil.printTranslatedMessage("user.utilsValidation.emailWarning");
        }

        return isValid;
    };

     public final Validation picValidation = link -> {
        boolean isValid = link.matches(RegExp.PIC_EXTENSION.getValue());

        if (!isValid) {
            dataFormatUtil.printTranslatedMessage("user.utilsValidation.picWarning");
        }

        return isValid;
    };

     public User createUser() {
        return User.userBuilder()
                .setLogin(dataFormatUtil.getUserInput("user.createUser.enterLogin", emptyValueValidation))
                .setPassword(
                        dataFormatUtil.encryptPassword(getConfirmedPassword())
                )
                .setName(dataFormatUtil.getUserInput("user.createUser.enterName", emptyValueValidation))
                .setLastName(dataFormatUtil.getUserInput("user.createUser.enterLastName", emptyValueValidation))
                .setEmail(dataFormatUtil.getUserInput("user.createUser.enterEmail", emailValidation))
                .setPhoto(dataFormatUtil.getUserInput("user.createUser.photo", picValidation))
                .setIsActive(true)
                .build();
    }

   public  String getConfirmedPassword(){
        String password = dataFormatUtil.getUserInput("user.createUser.enterPassword", this.emptyValueValidation);
        String passwordConfirm = dataFormatUtil.getUserInput("changePassword.confirmPassword.message", this.emptyValueValidation);

        if (password.equals(passwordConfirm)) {
           return password;

        } else {
            dataFormatUtil.printTranslatedMessage("changePassword.wrongConfirmPassword.warning");
            return getConfirmedPassword();
        }
    }

    public  DoCustomAction<User> deleteUserCustomAction = user -> {
        userRepository.remove(user);

        dataFormatUtil.printDivider();

        dataFormatUtil.printTranslatedMessage("user.deleteUser.success", new String[]{user.getName()});
        dataFormatUtil.waitForEnter();
    };

    public  DoCustomAction<User> updateUserCustomAction = user -> {

        user.setName(dataFormatUtil.getUserInput("user.createUser.enterName", this.emptyValueValidation));
        user.setLastName(dataFormatUtil.getUserInput("user.createUser.enterLastName", this.emptyValueValidation));
        user.setEmail(dataFormatUtil.getUserInput("user.createUser.enterEmail", this.emailValidation));
        user.setRoleId(this.getRoleID());
        user.setPhoto(dataFormatUtil.getUserInput("user.createUser.photo", this.picValidation));

        userRepository.update(user);
        dataFormatUtil.printDivider();
        dataFormatUtil.printTranslatedMessage("user.updateUser.success", new String[]{user.getName()});
    };
}
