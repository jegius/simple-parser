package ru.education.client.menu.commands.website;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.repository.WebSiteRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.SAVE_WEBSITE)
public class WebsiteSaveCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private WebSiteRepository webSiteRepository;

    public WebsiteSaveCommand() {
    }

    @Override
    public Command execute() {
        webSiteRepository.save();

        dataFormatUtil.printTranslatedMessage("website.saveWebsite.success");
        dataFormatUtil.waitForEnter();

        return CommandManager
                .getCommandByChoice(CommandsList.WEBSITE_REPOSITORY);
    }
}
