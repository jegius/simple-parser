package ru.education.client.menu.commands.user;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

@CommandMark(command = CommandsList.USER_REPOSITORY)
public class UserCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public UserCommand() {
    }

    @Override
    public Command execute() {
        CommandsList[] commandsOptions = {
                CommandsList.MAIN_MENU,
                CommandsList.CREATE_USER,
                CommandsList.DELETE_USER,
                CommandsList.UPDATE_USER,
                CommandsList.READ_USER,
                CommandsList.SAVE_USER,
        };
        dataFormatUtil.printOptions(commandsOptions);
        CommandsList choice = dataFormatUtil.selectCommand(commandsOptions);

        return CommandManager
                .getCommandByChoice(choice);

    }
}
