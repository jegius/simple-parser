package ru.education.client.menu.commands.user.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.parser.model.User;
import ru.education.client.repository.UserRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@CommandMark(command = CommandsList.READ_USER)
public class ReadUserCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private UserRepository userRepository;

    public ReadUserCommand() {
    }

    @Override
    public Command execute() {
        List<User> users = userRepository.get();
        if (users.isEmpty()) {
            dataFormatUtil.printTranslatedMessage("user.readUser.emptyWarning");
        } else {
            dataFormatUtil.printListItem(users);
        }
        return CommandManager
                .getCommandByChoice(CommandsList.USER_REPOSITORY);
    }
}
