package ru.education.client.menu.commands.targetparser.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.targetparser.CRUD.utils.TargetCommandUtils;
import ru.education.client.parser.model.Target;
import ru.education.client.repository.ParseEventRepository;
import ru.education.client.repository.TargetRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.utils.ValidationUtils;

@CommandMark(command = CommandsList.CREATE_TARGET)
public class CreateTargetCommand extends ThreadCommand {
    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private TargetCommandUtils targetCommandUtils;

    @Bind
    private ValidationUtils validationUtils;

    @Bind
    private ParseEventRepository parseEventRepository;

    @Bind
    private TargetRepository targetRepository;

    public CreateTargetCommand() {
    }

    @Override
    public Command execute() {

        if (parseEventRepository.get().isEmpty()) {
            dataFormatUtil.printTranslatedMessage("target.createTarget.createParseEvent");
            return CommandManager
                    .getCommandByChoice(CommandsList.PARSE_EVENT_REPOSITORY);
        } else {
            Target target = Target.targetBuilder()
                    .setParseEventId(targetCommandUtils.getParseEventIdForTarget())
                    .setName(dataFormatUtil.getUserInput(
                            "target.createTarget.enterName",
                            validationUtils.emptyValidation))
                    .setSecondUrl(dataFormatUtil.getUserInput(
                            "target.createTarget.enterUrl",
                            validationUtils.urlValidation))
                    .setHtmlTag(dataFormatUtil.getUserInput(
                            "target.createTarget.enterHtmlTag",
                            validationUtils.emptyValidation))
                    .setDescription(dataFormatUtil.getUserInput(
                            "target.createTarget.enterDescription",
                            validationUtils.emptyValidation))
                    .build();

            targetRepository.add(target);
        }

        return CommandManager
                .getCommandByChoice(CommandsList.TARGET_REPOSITORY);
    }
}
