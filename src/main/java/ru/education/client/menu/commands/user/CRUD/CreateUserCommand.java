package ru.education.client.menu.commands.user.CRUD;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.menu.commands.user.CRUD.utils.UserCommandUtils;
import ru.education.client.parser.model.User;
import ru.education.client.repository.RoleRepository;
import ru.education.client.repository.UserRepository;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;

import java.sql.SQLException;

@CommandMark(command = CommandsList.CREATE_USER)
public class CreateUserCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private UserCommandUtils userCommandUtils;

    @Bind
    private RoleRepository roleRepository;

    @Bind
    private UserRepository userRepository;

    public CreateUserCommand() {
    }

    @Override
    public Command execute() {

        if (roleRepository.get().isEmpty()) {
            dataFormatUtil.printTranslatedMessage("user.createUser.roleExistingWarning");

            return CommandManager
                    .getCommandByChoice(CommandsList.ROLE_REPOSITORY);
        } else {
            try {
                User user = userCommandUtils.createUser();
                userRepository.add(user);
                dataFormatUtil.printTranslatedMessage("user.createUser.success", new String[]{user.getName()});
            } catch (SQLException e) {
                return this;
            }
        }

        return CommandManager
                .getCommandByChoice(CommandsList.USER_REPOSITORY);
    }
}

