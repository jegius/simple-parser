package ru.education.client.menu;

import ru.education.client.threads.user.UserThread;

public interface Command {

    Command execute(UserThread thread);
}
