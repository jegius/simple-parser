package ru.education.client.menu;

import ru.education.client.threads.user.UserThread;

public class ConsoleMenu {

    private Command command;

    public ConsoleMenu(Command startCommand) {
        this.command = startCommand;
    }

    public void run(UserThread thread){
        executeCommand(thread);
    }

    private void executeCommand(UserThread thread){
        while (command != null) {
            command = command.execute(thread);
        }
    }
}
