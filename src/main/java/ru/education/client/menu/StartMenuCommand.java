package ru.education.client.menu;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.CommandMark;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.commands.ThreadCommand;
import ru.education.client.services.CommandManager;
import ru.education.client.utils.DataFormatUtil;


@CommandMark(command = CommandsList.START_MENU)
public class StartMenuCommand extends ThreadCommand {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public StartMenuCommand() {
    }

    @Override
    public Command execute() {

        CommandsList[] commandsOptions = {
                CommandsList.EXIT,
                CommandsList.LOGIN,
                CommandsList.REGISTRATION,
                CommandsList.LANGUAGE,
        };

        dataFormatUtil.printOptions(commandsOptions);
        CommandsList choose = dataFormatUtil.selectCommand(commandsOptions);

        return CommandManager
                .getCommandByChoice(choose);
    }
}
