package ru.education.client.services.session;

public enum SessionAccessKeys {
    USER("User");

    private String value;

    SessionAccessKeys(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
