package ru.education.client.services.session;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Service;
import ru.education.client.parser.model.Session;
import ru.education.client.parser.model.User;
import ru.education.client.repository.SessionRepository;

import java.util.UUID;

@Service
public class ConsoleUserSessionService {

    @Bind
    private SessionRepository sessionRepo;

    public ConsoleUserSessionService() {
    }

    private Session currentSession;

    public UUID initSession(User user) {

        Session session = Session
                .sessionBuilder()
                .setData(SessionAccessKeys.USER.getValue(), user)
                .build();

        session = sessionRepo.add(session);
        sessionRepo.save();
        return session.getId();
    }

    public void deleteSession(UUID sessionId) {
        sessionRepo
                .get()
                .remove(sessionRepo.getById(sessionId));
        sessionRepo.save();
    }
}
