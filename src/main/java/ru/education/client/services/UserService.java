package ru.education.client.services;

import ru.education.client.Configuration;
import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Service;
import ru.education.client.parser.model.User;
import ru.education.client.repository.UserRepository;
import ru.education.client.utils.DataFormatUtil;

@Service
public class UserService {

    public UserService() {
    }

    @Bind
    private UserRepository userRepository;

    @Bind
    private DataFormatUtil dataFormatUtil;

    public boolean checkPassword(String login, String password) {
        User searchedUser = userRepository
                .getUserByLogin(login);

        if (searchedUser != null) {
            return dataFormatUtil
                    .getPasswordEncryptor()
                    .checkPassword(password, searchedUser.getPassword());
        } else {
            return false;
        }
    }
}
