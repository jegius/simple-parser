package ru.education.client.services;

import ru.education.client.annotations.*;
import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.services.servicemanager.InjectionManager;

import java.util.HashMap;
import java.util.List;

public class CommandManager {

    private static HashMap<CommandsList, Command> commandsHolder;
    private static List<Object> commands;

    public CommandManager() {
    }

    public static Command getCommandByChoice(CommandsList commandsList) {
        return commandsHolder.get(commandsList);
    }

    static {
        commandsHolder = new HashMap<>();
        commands = InjectionManager.getBeansByAnnotation(CommandMark.class);
        commands.forEach(
                command -> commandsHolder
                        .put(command.getClass()
                                        .getAnnotation(CommandMark.class)
                                        .command(),
                                (Command) command)
        );
    }
}
