package ru.education.client.services.localization;

import ru.education.client.Configuration;
import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Service;
import ru.education.client.services.OnInitService;
import ru.education.client.settings.SettingsService;
import ru.education.client.utils.DataFormatUtil;
import ru.education.client.utils.RegExp;

import java.io.*;
import java.util.*;

import static java.util.stream.Collectors.toMap;

@Service
public class LocalizationService extends OnInitService {

    @Bind
    private SettingsService settingsService;

    @Bind
    private DataFormatUtil dataFormatUtil;

    private static final ClassLoader loader = Thread.currentThread().getContextClassLoader();

    private static Map<String, Properties> values;

    private static final String path = Configuration.getProperty("localization.path");

    private static String currentLocalization;

    public LocalizationService() {
    }

    private  String mapKey(String line) {
        return line.replaceAll(RegExp.LOCALIZATION_PROPERTY_KEY.getValue(), "$1");
    }

    private  Properties mapValue(String line) {
        return getLocalization(path + "/" + line);
    }

    public void setCurrentLocalization(String localization) {
        if (values.containsKey(localization)) {
            currentLocalization = localization;
        } else {
            dataFormatUtil.printTranslatedMessage("localizationService.wrongLocale.warning");
        }
    }

    public List<String> getAvailableLocales() {
        return new ArrayList<>(values.keySet());
    }

    public String translate(String localizationKey) {
        return Optional.ofNullable(
                values
                        .get(currentLocalization)
                        .getProperty(localizationKey)
        )
                .orElse(localizationKey);
    }

    private  Properties getLocalization(String localizationPath) {

        Properties properties = new Properties();

        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream(localizationPath)) {
            properties.load(inputStream);
        } catch (IOException ioException) {
            dataFormatUtil.printTranslatedMessage("localizationService.fileError.warning");
        }
        return properties;
    }

    @Override
    public void init() {
        String defaultLocale = settingsService
                .getSettingValue("setting.localization.locale.default");

        try (
                InputStream inputStream = loader.getResourceAsStream(path);
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {

            values = bufferedReader
                    .lines()
                    .collect(toMap(this::mapKey, this::mapValue));

            if (values.containsKey(defaultLocale)) {
                currentLocalization = defaultLocale;
            } else {
                currentLocalization = values
                        .keySet()
                        .toArray()[0]
                        .toString();
                System.out.println("No default localization");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
