package ru.education.client.services.servicemanager;

import org.reflections.Reflections;
import ru.education.client.annotations.*;
import ru.education.client.repository.Repository;
import ru.education.client.services.OnInitService;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class InjectionManager {

    private static Reflections reflections;

    private static Map<String, Object> beans;

    public static List<Object> getBeansByAnnotation(Class<? extends Annotation> annotation) {
        return beans
                .values()
                .stream()
                .filter(bean -> bean
                        .getClass()
                        .getAnnotation(annotation) != null)
                .collect(Collectors.toList()
                );
    }

    private static String mapKey(Class<?> providedClass) {
        return providedClass.getName();
    }

    private static Object createInstance(Class<?> providedClass) {
        try {
            return providedClass.getConstructor().newInstance();
        } catch (InstantiationException |
                IllegalAccessException |
                InvocationTargetException |
                NoSuchMethodException e) {
            System.out.println(providedClass.getName());
            return null;
        }
    }

    private static void createInstance() {
        reflections = new Reflections("ru.education.client");
        beans = Stream.of(
                reflections.getTypesAnnotatedWith(Service.class),
                reflections.getTypesAnnotatedWith(CommandMark.class),
                reflections.getTypesAnnotatedWith(Util.class),
                reflections.getTypesAnnotatedWith(RepositoryMark.class),
                reflections.getTypesAnnotatedWith(DAO.class)
        )
                .flatMap(Collection::stream)
                .collect(toMap(InjectionManager::mapKey,
                        InjectionManager::createInstance));
    }

    private static void inject() {
        beans
                .values()
                .forEach(object -> Stream.of(object.getClass().getDeclaredFields())
                        .filter(field -> field.getAnnotation(Bind.class) != null)
                        .forEach(field -> {
                            try {
                                field.setAccessible(true);
                                field.set(object, beans.get(field.getType().getName()));
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }));
    }

    private static void loadDataFromRepository() {
        beans.values().forEach(providedBean ->
                Stream.of(providedBean
                        .getClass()
                        .getInterfaces())
                        .forEach(providedInterface -> {
                            if (providedInterface.getName().equals(Repository.class.getName())) {
                                Repository beanRepository = (Repository) providedBean;
                                beanRepository.load();
                            }
                        })
        );
    }

    private static void initService() {
        beans.values().stream().filter(object ->
                object
                        .getClass()
                        .getSuperclass()
                        .equals(OnInitService.class)
        ).forEach(service -> {
            OnInitService castService = (OnInitService) service;
            castService.init();
        });
    }

    static {
        createInstance();
        inject();
        loadDataFromRepository();
        initService();
    }
}
