package ru.education.client;

import ru.education.client.consts.Constant;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public final class Configuration {

    private static Properties properties = new Properties();

    static {
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream(Constant.PROPERTY_PATH.getValue())) {

            if (inputStream == null) throw new NullPointerException("No inputStream!");

            properties.load(inputStream);
        } catch (IOException e) {
            throw new IllegalStateException("FATAL! Cannot read " + Constant.PROPERTY_PATH + "!");
        }
    }

    public static String getProperty(String name) {
        return properties.getProperty(name);
    }

    public static List<String> getPropertiesKeys() {
        return new ArrayList<>(properties.stringPropertyNames());
    }
}
