package ru.education.client.passwordinput;

import ru.education.client.Configuration;
import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Service;
import ru.education.client.consts.Constant;
import ru.education.client.menu.commands.user.CRUD.utils.UserCommandUtils;
import ru.education.client.utils.DataFormatUtil;

@Service
public class PasswordInputMode {

    @Bind
    private UserCommandUtils userCommandUtils;

    @Bind
    private DataFormatUtil dataFormatUtil;

    /**
     * Provides to enter the open or hidden password,
     * depending on the selected password input mode:
     * 'dev' (for developers), another one (for users)
     * for password.input.mode in config.properties
     *
     * @return password
     */
    public String getPasswordInputByType() {
        String passwordInputValue = Configuration.getProperty(Constant.PASSWORD_INPUT_MODE.getValue());

        return passwordInputValue
                .equals(PasswordInputKind.PASSWORD_INPUT_DEV.getValue())
                ? dataFormatUtil.getUserInput(
                "loginMenu.option.enterPassword",
                userCommandUtils.emptyValueValidation)
                : dataFormatUtil.getHiddenPassword(
                "loginMenu.option.enterPassword",
                userCommandUtils.emptyValueValidation);
    }
}
