package ru.education.client.passwordinput;

public enum PasswordInputKind {

    PASSWORD_INPUT_DEV("dev");

    private String value;

    PasswordInputKind(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}