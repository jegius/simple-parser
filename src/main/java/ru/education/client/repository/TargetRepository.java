package ru.education.client.repository;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.RepositoryMark;
import ru.education.client.consts.Constant;
import ru.education.client.parser.model.Target;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@RepositoryMark
public class TargetRepository implements Repository<Target> {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public TargetRepository() {
    }

    private List<Target> targets;

    @Override
    public List<Target> get() {
        return targets;
    }

    public Target getById(String id) {
        return targets
                .stream()
                .filter(target -> target.getId().equals(id))
                .findFirst()
                .get();
    }

    @Override
    public Target add(Target object) {
        if (object == null) {
            return null;
        }

        Target target = new Target(object);
        target.setId(getGeneratedId());
        targets.add(target);

        return target;
    }

    @Override
    public Target update(Target object) {
        if (object == null) {
            return null;
        }

        Target target = new Target(object);
        remove(target);
        targets.add(target);

        return target;
    }

    @Override
    public void remove(Target object) {
        if (object == null) {
            return;
        }

        targets.remove(getById(object.getId()));
    }

    @Override
    public void removeAll() {
        targets.removeAll(targets);
    }

    @Override
    public void save() {
        dataFormatUtil.saveList(targets, Constant.TARGET_PATH.toString());
    }

    @Override
    public void load() {
        targets = dataFormatUtil.loadList(Constant.TARGET_PATH.toString(), Target.class);
    }
}
