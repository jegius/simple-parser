package ru.education.client.repository;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.RepositoryMark;
import ru.education.client.consts.Constant;
import ru.education.client.parser.model.WebSite;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@RepositoryMark
public class WebSiteRepository implements Repository<WebSite> {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public WebSiteRepository() {
    }

    private List<WebSite> webSites;

    @Override
    public List<WebSite> get() {
        return webSites;
    }

    public WebSite getById(String id) {
        return webSites
                .stream()
                .filter(webSite -> webSite.getId().equals(id))
                .findFirst()
                .get();
    }

    @Override
    public WebSite add(WebSite object) {
        if (object == null) {
            return null;
        }

        WebSite webSite = new WebSite(object);
        webSite.setId(getGeneratedId());
        webSites.add(webSite);

        return webSite;
    }

    @Override
    public WebSite update(WebSite object) {
        if (object == null) {
            return null;
        }

        WebSite webSite = new WebSite(object);
        remove(webSite);
        webSites.add(webSite);

        return webSite;
    }

    @Override
    public void remove(WebSite object) {
        if (object == null) {
            return;
        }

        webSites.remove(getById(object.getId()));
    }

    @Override
    public void removeAll() {
        webSites.removeAll(webSites);
    }

    @Override
    public void save() {
        dataFormatUtil.saveList(webSites, Constant.WEBSITES_PATH.toString());
    }

    @Override
    public void load() {
        webSites = dataFormatUtil.loadList(Constant.WEBSITES_PATH.toString(), WebSite.class);
    }
}
