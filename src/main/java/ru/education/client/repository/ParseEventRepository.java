package ru.education.client.repository;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.RepositoryMark;
import ru.education.client.consts.Constant;
import ru.education.client.parser.model.ParseEvent;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;
import java.util.UUID;

@RepositoryMark
public class ParseEventRepository implements Repository<ParseEvent> {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public ParseEventRepository() {
    }

    private List<ParseEvent> parseEvents;

    @Override
    public List<ParseEvent> get() {
        return parseEvents;
    }

    public ParseEvent getById(UUID id) {
        return parseEvents
                .stream()
                .filter(parseEvent -> parseEvent.getId().equals(id))
                .findFirst()
                .get();
    }

    @Override
    public ParseEvent add(ParseEvent object) {
        if (object == null) {
            return null;
        }

        ParseEvent parseEvent = new ParseEvent(object);
        parseEvent.setId(getGeneratedId());
        parseEvents.add(parseEvent);

        return parseEvent;
    }

    @Override
    public ParseEvent update(ParseEvent object) {
        if (object == null) {
            return null;
        }

        ParseEvent parseEvent = new ParseEvent(object);
        remove(parseEvent);
        parseEvents.add(parseEvent);

        return parseEvent;
    }

    @Override
    public void remove(ParseEvent object) {
        if (object == null) {
            return;
        }

        parseEvents.remove(getById(object.getId()));
    }

    @Override
    public void removeAll() {
        parseEvents.removeAll(parseEvents);
    }

    @Override
    public void save() {
        dataFormatUtil.saveList(parseEvents, Constant.PARSE_EVENTS_PATH.toString());
    }

    @Override
    public void load() {
        parseEvents = dataFormatUtil.loadList(Constant.PARSE_EVENTS_PATH.toString(), ParseEvent.class);
    }
}
