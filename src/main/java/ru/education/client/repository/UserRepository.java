package ru.education.client.repository;

import ru.education.client.DAO.UserDAO;
import ru.education.client.annotations.Bind;
import ru.education.client.annotations.RepositoryMark;
import ru.education.client.consts.Constant;
import ru.education.client.parser.model.User;
import ru.education.client.utils.DataFormatUtil;

import java.sql.SQLException;
import java.util.List;

@RepositoryMark
public class UserRepository implements Repository<User> {

    @Bind
    private UserDAO userDAO;

    @Bind
    private DataFormatUtil dataFormatUtil;

    public UserRepository() {
    }

    private List<User> users;

    public User getUserByLogin(String login) {
        return userDAO.getUserByLogin(login);
    }

    @Override
    public List<User> get() {
        return users;
    }

    @Override
    public User add(User object) throws SQLException {
        if (object == null) {
            return null;
        }
       return userDAO.addUser(object);
    }

    @Override
    public User update(User object) {
        if (object == null) {
            return null;
        }
        return userDAO.updateUser(object);
    }

    @Override
    public void remove(User object) {
        if (object == null) {
            return;
        }
        users.remove(getById(object.getId()));
    }

    @Override
    public void removeAll() {
        users.removeAll(users);
    }


    @Override
    public void save() {
        dataFormatUtil.saveList(users, Constant.USERS_PATH.toString());
    }

    @Override
    public void load() {
        users = dataFormatUtil.loadList(Constant.USERS_PATH.toString(), User.class);
    }
}
