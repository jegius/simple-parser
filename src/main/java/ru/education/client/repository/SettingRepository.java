package ru.education.client.repository;

import ru.education.client.DAO.SettingDAO;
import ru.education.client.annotations.Bind;
import ru.education.client.annotations.RepositoryMark;
import ru.education.client.consts.Constant;
import ru.education.client.parser.model.Setting;

import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@RepositoryMark
public class SettingRepository implements Repository<Setting> {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private SettingDAO settingDAO;

    public SettingRepository() {
    }

    private List<Setting> options;


    public List<Setting> get() {
        return settingDAO.getAllSettings();
    }


    public Setting add(Setting object) {
        if (object == null) {
            return null;
        }
        return settingDAO.addSetting(object);
    }


    public Setting update(Setting object) {
        if (object == null) {
            return null;
        }
        Setting option = new Setting(object);
        remove(option);
        options.add(option);
        return option;
    }


    public void remove(Setting object) {
        if (object == null) {
            return;
        }
        options.remove(object.getId());
    }

    public void removeAll() {
        options.removeAll(options);
    }

    @Override
    public void save() {
        dataFormatUtil.saveList(options, Constant.SETTINGS_PATH.toString());
    }

    @Override
    public void load() {
        options = dataFormatUtil.loadList(Constant.SETTINGS_PATH.toString(), Setting.class);
    }
}
