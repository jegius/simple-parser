package ru.education.client.repository;

import ru.education.client.parser.model.IdentifiedItem;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public interface Repository<T extends IdentifiedItem> {

    default T getById(UUID id) {
        return this.get()
                .stream()
                .filter(item -> item.getId().equals(id))
                .findFirst()
                .get();
    }

    default UUID getGeneratedId() {
        return UUID.randomUUID();
    }

    /**
     * Returns all objects
     * @return Objects list
     */
    List<T> get();

    /**
     * Adds new object
     * @param object Object to add
     * @return Added object
     */
    T add(T object) throws SQLException;

    /**
     * Updates object
     * @param object Object to update
     * @return Updated object
     */
    T update(T object);

    /**
     * Removes object
     * @param object Object to remove
     */
    void remove(T object);

    /**
     * Remove all objects from repo
     */
    void removeAll();

    /**
     * Saves data to file
     */
    void save();

    /**
     * Loads data from file
     */
    void load();
}