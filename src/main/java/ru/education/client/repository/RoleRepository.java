package ru.education.client.repository;

import ru.education.client.DAO.RoleDAO;
import ru.education.client.annotations.Bind;
import ru.education.client.annotations.RepositoryMark;
import ru.education.client.consts.Constant;
import ru.education.client.parser.model.Role;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@RepositoryMark
public class RoleRepository implements Repository<Role> {

    @Bind
    private DataFormatUtil dataFormatUtil;

    @Bind
    private RoleDAO roleDAO;

    private static List<Role> roles;

    public Role getRoleByName(String name) {
        return roleDAO.getRoleByName(name);
    }

    @Override
    public List<Role> get() {
        return roleDAO.getAllRoles();
    }

    @Override
    public Role add(Role object) {
        if (object == null) {
            return null;
        }
        return roleDAO.addRole(object);
    }

    @Override
    public Role update(Role object) {
        if (object == null) {
            return null;
        }
        return roleDAO.updateRole(object);
    }

    @Override
    public void removeAll() {
        roles.removeAll(roles);
    }

    @Override
    public void remove(Role object) {
        if (object == null) {
            return;
        }
        roles.remove(getById(object.getId()));
    }

    @Override
    public void save() {
        dataFormatUtil.saveList(roles, Constant.ROLES_PATH.toString());
    }

    @Override
    public void load() {
        roles = dataFormatUtil.loadList(Constant.ROLES_PATH.toString(), Role.class);
    }
}
