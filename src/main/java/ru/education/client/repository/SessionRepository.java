package ru.education.client.repository;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.RepositoryMark;
import ru.education.client.consts.Constant;
import ru.education.client.parser.model.Session;
import ru.education.client.utils.DataFormatUtil;

import java.util.List;

@RepositoryMark
public class SessionRepository implements Repository<Session> {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public SessionRepository() {
    }

    private List<Session> sessions;

    @Override
    public List<Session> get() {
        return sessions;
    }

    @Override
    public Session add(Session object) {
        if (object == null) {
            return null;
        }
        Session session = new Session(object);
        session.setId(getGeneratedId());
        sessions.add(session);
        return session;
    }

    @Override
    public Session update(Session object) {
        if (object == null) {
            return null;
        }
        Session session = new Session(object);
        remove(session);
        sessions.add(session);
        return session;
    }

    @Override
    public void remove(Session object) {
        if (object == null) {
            return;
        }
        sessions.remove(getById(object.getId()));
    }

    @Override
    public void removeAll() {
        sessions.removeAll(sessions);
    }

    @Override
    public void save() {
        dataFormatUtil.saveList(sessions, Constant.SESSION_PATH.toString());
    }

    @Override
    public void load() {
        sessions = dataFormatUtil.loadList(Constant.SESSION_PATH.toString(), Session.class);
    }
}
