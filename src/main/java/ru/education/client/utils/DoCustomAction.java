package ru.education.client.utils;

@FunctionalInterface
public interface DoCustomAction<T> {
    void doAction(T object);
}
