package ru.education.client.utils;

public enum RegExp {

    WEBSITE("^((https?):\\/\\/)?([w]{3}\\.)?([a-zA-Z0-9\\.]+)(\\.[a-z]+)$"),
    SETTINGS("^setting\\..+"),
    EMAIL("[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z0-9]+"),
    PIC_EXTENSION("[a-z0-9:\\.\\-/]+\\.(gif|jpe?g|tiff?|png|webp|bmp)"),
    LOCALIZATION_PROPERTY_KEY("([a-z]{2}-[A-Z]{2}).+"),
    VALID_NUMBER("^[0-9]+$");

    private String value;

    RegExp(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}