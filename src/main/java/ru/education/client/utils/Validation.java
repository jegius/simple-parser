package ru.education.client.utils;

@FunctionalInterface
public interface Validation {
    boolean isValid(String value);
}
