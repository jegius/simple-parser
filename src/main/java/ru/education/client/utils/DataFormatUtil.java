package ru.education.client.utils;

import org.jasypt.util.password.StrongPasswordEncryptor;
import ru.education.client.Configuration;
import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Service;
import ru.education.client.annotations.Util;
import ru.education.client.consts.CommandsList;
import ru.education.client.services.localization.LocalizationService;
import ru.education.client.menu.Command;
import ru.education.client.parser.model.IdentifiedItem;
import ru.education.client.repository.Repository;
import ru.education.client.utils.DoCustomAction;
import ru.education.client.utils.JSONUtils;
import ru.education.client.utils.RegExp;
import ru.education.client.utils.Validation;

import java.io.Console;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

@Util
public class DataFormatUtil {

    @Bind
    private LocalizationService localizationService;

    private final StrongPasswordEncryptor strongPasswordEncryptor = new StrongPasswordEncryptor();

    public StrongPasswordEncryptor getPasswordEncryptor() {
        return strongPasswordEncryptor;
    }

    public Scanner getScanner() {
        return new Scanner(System.in);
    }

    public void printDivider() {
        System.out.println("|===================================|");
    }

    public void waitForEnter() {
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printPrompt() {
        System.out.print(">: ");
    }

    public void printOptions(CommandsList[] commandOptions) {
        printDivider();
        IntStream.
                range(0, commandOptions.length)
                .forEach(optionIndex ->
                        printTranslatedMessage(
                                commandOptions[optionIndex].toString(),
                                new String[]{String.valueOf(optionIndex)})
                );
        printDivider();

    }

    public void printListItem(List<?> listItems) {
        IntStream.range(0, listItems
                .size())
                .forEach(index ->
                        printTranslatedMessage("universal.message.divided",
                                new String[]{
                                        String.valueOf(index + 1),
                                        listItems.get(index).toString()
                                })
                );
    }

    public CommandsList selectCommand(CommandsList[] allOptions) {
        String userInput = getUserInput(
                null,
                value -> {
                    boolean isValid = value.matches(RegExp.VALID_NUMBER.getValue());

                    if (!isValid) {
                        printTranslatedMessage("menuUtils.getChoice.warning");
                    }
                    return isValid;
                });

        int choose = Integer.parseInt(userInput);
        if (allOptions.length <= choose) {
            System.out.println("No switch command!");
            return selectCommand(allOptions);
        }
        return allOptions[choose];
    }

    public Command checkItemExistingAndDoCustom(
            Command command,
            DoCustomAction customAction,
            Repository<? extends IdentifiedItem> providedRepository
    ) {
        String userInput = getUserInput(
                "menuUtils.getChoice.message",
                value -> {
                    boolean isValid = value.matches(RegExp.VALID_NUMBER.getValue());

                    if (!isValid) {
                        printTranslatedMessage("menuUtils.getChoice.warning");
                    }

                    return isValid;
                });
        this.printDivider();

        if (providedRepository.get().isEmpty()) {
            printTranslatedMessage("menuUtils.emptyRepository.warning");
            return command;
        }
        IdentifiedItem identifiedItem;
        try {
            int userChoice = Integer.parseInt(userInput) - 1;
            identifiedItem = providedRepository
                    .get()
                    .get(userChoice);
            customAction.doAction(identifiedItem);
        } catch (IndexOutOfBoundsException | NullPointerException e) {
            printTranslatedMessage("menuUtils.missingElement.warning");
            checkItemExistingAndDoCustom(command, customAction, providedRepository);
        }

        return command;
    }

    public String getUserInput(
            String message,
            Validation validation
    ) {
        if (message != null) {
            printTranslatedMessage(message);
        }

        printPrompt();

        String value = getScanner().nextLine();

        if (!validation.isValid(value)) {
            return getUserInput(message, validation);
        }

        return value;
    }

    /**
     * This method necessary for input user's password
     * Entered symbols will be hidden for users
     *
     * @return String password
     */
    public String getHiddenPassword(
            String message,
            Validation validation
    ) {
        Console console = System.console();
        if (message != null) {
            printTranslatedMessage(message);
        }

        printPrompt();

        char[] passwordArray = console.readPassword();
        String password = new String(passwordArray);

        if (!validation.isValid(password)) {
            return getHiddenPassword(message, validation);
        }
        return password;
    }

    public void printTranslatedMessage(String localisationKey) {
        System.out.println(
                localizationService
                        .translate(localisationKey)
        );
    }

    public void printTranslatedMessage(String localizationKey, String[] args) {
        String localizedMessage = localizationService
                .translate(localizationKey);

        localizedMessage = IntStream
                .range(0, args.length)
                .mapToObj(String::valueOf)
                .reduce(localizedMessage, (message, index) -> message
                        .replaceAll(
                                "\\{" + index + "\\}",
                                args[Integer.parseInt(index)]
                        ));

        System.out.println(localizedMessage);
    }

    public void saveList(List<? extends IdentifiedItem> repository, String filePath) {
        try {
            JSONUtils.writeToFile(
                    Configuration.getProperty(filePath),
                    repository);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List loadList(String filePath, Class<? extends IdentifiedItem> itemClass) {
        List repository;
        try {
            repository = JSONUtils.readListFromFile(Configuration.getProperty(filePath), itemClass);
        } catch (IOException ioException) {
            repository = new ArrayList<>();
            printTranslatedMessage("repository.fileRead.warning");
        }
        return repository;
    }

    public String encryptPassword(String password) {
        return getPasswordEncryptor().encryptPassword(password);
    }
}
