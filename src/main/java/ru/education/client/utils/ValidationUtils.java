package ru.education.client.utils;

import ru.education.client.annotations.Bind;
import ru.education.client.annotations.Util;

@Util
public class ValidationUtils {

    @Bind
    private DataFormatUtil dataFormatUtil;

    public final Validation emptyValidation = value -> {
        boolean isEmpty = value.isEmpty();

        if (isEmpty) {
            dataFormatUtil.printTranslatedMessage("validationUtils.emptyValidation.message");
        }

        return !isEmpty;
    };

    public final Validation urlValidation = value -> {
        boolean isValid = value.matches(RegExp.WEBSITE.getValue());

        if (!isValid) {
            dataFormatUtil.printTranslatedMessage("validationUtils.urlValidation.message");
        }

        return isValid;
    };
}
