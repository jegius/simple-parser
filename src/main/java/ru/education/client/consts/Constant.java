package ru.education.client.consts;

public enum Constant {
    BASE_PATH("out"),
    PROPERTY_PATH("config.properties"),
    PARSE_EVENTS_PATH("path.parseEvents.fileName"),
    TARGET_PATH("path.targets.fileName"),
    WEBSITES_PATH("path.websites.fileName"),
    SESSION_PATH("path.session.fileName"),
    ROLES_PATH("path.roles.fileName"),
    USERS_PATH("path.users.fileName"),
    SETTINGS_PATH("path.setting.fileName"),
    FIRST_ELEMENT_OF_LIST("1"),
    DEFAULT_ADMIN_ROLE("ADMIN"),
    DEFAULT_USER_ROLE("USER"),
    PASSWORD_INPUT_MODE("password.input.mode"),
    EMPTY_STRING(""),
    PARSE_MAPPING_PATH("parseMappings.fileName");

    private String value;

    Constant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
