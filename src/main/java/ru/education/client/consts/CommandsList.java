package ru.education.client.consts;

public enum CommandsList {

    MAIN_MENU("common.option.mainMenu"),

    /**
     * Main Menu
     */
    START_MENU("mainMenu.option.exit"),
    START_PARSING("mainMenu.option.startParsing"),
    ROLE_REPOSITORY("mainMenu.option.roleRepository"),
    WEBSITE_REPOSITORY("mainMenu.option.websiteRepository"),
    TARGET_REPOSITORY("mainMenu.option.targetRepository"),
    PARSE_MAPPING_REPOSITORY("Parse Mapping Repository"),
    USER_REPOSITORY("mainMenu.option.userRepository"),
    PARSE_EVENT_REPOSITORY("mainMenu.option.parseEventRepository"),
    PREFERENCES("mainMenu.option.preferences"),

    /**
     * Parse Menu
     */
    UPDATE_PARSE_EVENT("showParseEvent.option.updateParseEvent"),
    READ_PARSE_EVENT("showParseEvent.option.readParseEvent"),
    DELETE_PARSE_EVENT("showParseEvent.option.deleteParseEvent"),
    CREATE_PARSE_EVENT("showParseEvent.option.createParseEvent"),
    SAVE_PARSE_EVENT("showParseEvent.option.saveParseEvent"),

    /**
     * Role Menu
     */
    UPDATE_ROLE("showRole.option.updateRole"),
    READ_ROLE("showRole.option.readRole"),
    DELETE_ROlE("showRole.option.deleteRole"),
    CREATE_ROLE("showRole.option.createRole"),
    SAVE_ROLE("showRole.option.saveRole"),

    /**
     * Target Menu
     */
    UPDATE_TARGET("showTarget.option.updateTarget"),
    READ_TARGET("showTarget.option.readTarget"),
    DELETE_TARGET("showTarget.option.deleteTarget"),
    CREATE_TARGET("showTarget.option.createTarget"),
    SAVE_TARGET("showTarget.option.saveTarget"),

    /**
     * User Menu
     */
    UPDATE_USER("showUser.option.updateUser"),
    READ_USER("showUser.option.readUser"),
    DELETE_USER("showUser.option.deleteUser"),
    CREATE_USER("showUser.option.createUser"),
    SAVE_USER("showUser.option.saveUser"),

    /**
     * Website Menu
     */
    UPDATE_WEBSITE("showWebsite.option.updateWebsite"),
    READ_WEBSITE("showWebsite.option.readWebsite"),
    DELETE_WEBSITE("showWebsite.option.deleteWebsite"),
    CREATE_WEBSITE("showWebsite.option.createWebsite"),
    SAVE_WEBSITE("showWebsite.option.saveWebsite"),

    /**
     * Start Menu
     */
    LOGIN("startMenu.menu.login"),
    REGISTRATION("startMenu.menu.registration"),
    LOG_OUT("mainMenu.option.logOut"),

    /**
     * Preferences Menu
     */
    LANGUAGE("preferences.option.language"),
    CHANGE_PASSWORD("preferences.option.changePassword"),
    CHANGE_PHOTO("preferences.option.changePhoto"),
    EXIT("startMenu.option.exit");

    private String value;

    CommandsList(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
