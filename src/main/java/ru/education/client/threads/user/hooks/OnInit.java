package ru.education.client.threads.user.hooks;

import ru.education.client.consts.CommandsList;
import ru.education.client.menu.Command;
import ru.education.client.menu.ConsoleMenu;
import ru.education.client.services.CommandManager;
import ru.education.client.threads.user.UserThread;

public class OnInit implements Runnable {

    private final UserThread userThread;

    private Command startMenu;

    public OnInit(UserThread userThread) {
        this.userThread = userThread;
        this.startMenu = CommandManager
                .getCommandByChoice(CommandsList.MAIN_MENU);
    }

    @Override
    public void run() {
        ConsoleMenu menu = new ConsoleMenu(startMenu);
        menu.run(userThread);
    }
}
