package ru.education.client.threads.user;

import ru.education.client.threads.user.hooks.OnDestroy;
import ru.education.client.threads.user.hooks.OnInit;

import java.util.UUID;

public class UserThread extends Thread {

    public UUID getSessionId() {
        return sessionId;
    }

    private UUID sessionId;

    public UserThread(UUID sessionId) {
        super(sessionId.toString());
        this.sessionId = sessionId;
    }

    @Override
    public void run() {
        try {
         new OnInit(this).run();
        } finally {
            new OnDestroy(this).run();
        }
    }
}
