package ru.education.client.threads.user.hooks;

import ru.education.client.App;
import ru.education.client.annotations.Bind;
import ru.education.client.services.session.ConsoleUserSessionService;
import ru.education.client.threads.user.UserThread;

public class OnDestroy implements Runnable {

    @Bind
    private ConsoleUserSessionService consoleUserSessionService;
    private final UserThread userThread;

    public OnDestroy(UserThread userThread) {
        this.userThread = userThread;
    }

    @Override
    public void run() {
        consoleUserSessionService
                .deleteSession(userThread.getSessionId());

        App.run();
    }
}
